# -*- coding: utf-8 -*-
from mongoengine import Document
from mongoengine import StringField
from mongoengine import DateField


# modelo da aplicacao
class Paciente(Document):
    """
        classe responsavel
        por provisionar
        um modelo representativo
        da entidade paciente
        utilizada no sistema
    """

    nome = StringField(max_length=200, required=True)
    endereco = StringField(max_length=200, required=True)
    telefone = StringField(max_length=200, required=True)
    email = StringField(max_length=200, required=True)
    nascimento = DateField()
    regiao = StringField(max_length=20, required=True)
    estado = StringField(max_length=5, required=True)
    sexo = StringField(max_length=10, required=True)
    condicao = StringField(max_length=500, required=True)
    medicamento_controlado = StringField(max_length=5, required=True)
    nome_medicamento = StringField(max_length=200, required=True)
