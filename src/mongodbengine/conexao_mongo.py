# -*- coding: utf-8 -*-
from mongoengine import connect



def obter_conexao():
    """
    a function obter obter_conexao e responsavel
    por devolver uma conexao com banco mongodb
    :return: connect() {conexao com mongodb}
    obs: alterar url para coincidir com sua instalação do mongodb
    @author cristiano alexandre de faria
    """
    _SRV = 'mongodb://admin:admin@127.0.0.1:27017/?authSource=admin'
    try:
        conexao = connect(db='posto_saude', host=_SRV)
    except Exception as e:
        print('falha ao obter conexao ' + e)
    else:
        return conexao
