#!/usr/bin/python
# -*- coding: utf-8 -*-
import wx
from wx.adv import SplashScreen, DatePickerCtrl
import os
from src.mongodbengine.models.paciente import Paciente
from src.mongodbengine.conexao_mongo import obter_conexao
from mongoengine import disconnect
import datetime


# classe janela


class MainWindow(wx.Frame):
    """
    Construtor
    """

    def __init__(self):
        # temos que chamar o construtor da superclasse
        wx.Frame.__init__(
            self, None, size=(800, 600),  # obj self, janela pai e tamanho
            title="Posto de Saúde",  # titulo
            style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER | wx.STAY_ON_TOP  # desativa o redimensionamento
        )
        """
        nota não funciona em gtk ubuntu / apenas kde e windows-mac
        """
        icone_salvar = os.getcwd() + "/save.png"  # arquivo icone btn salvar
        icone_limpar = os.getcwd() + "/clean.png"  # arquivo icone btn salvar
        icone_atualizar = os.getcwd() + "/update.png"  # arquivo icone atualizar
        icone_deletar = os.getcwd() + "/delete.png" # arquivo icone deletar pylint: disable=unused
        icone = os.getcwd() + "/icon.png"  # carrega o arquivo de imagem
        icone_aplicacao = wx.Icon(icone)  # cria o objeto Icon
        self.SetIcon(icone_aplicacao)  # define o icone da aplicacao

        # painel
        painel = wx.Panel(self, -1)

        # definimos o layout da janela vertical
        dimensionador = wx.BoxSizer(wx.VERTICAL)

        # definimos o dimensionador da janela
        painel.SetSizer(dimensionador)

        # caixa estatica com campos do form
        wx.StaticBox(painel, -1, 'Cadastro de pacientes', (8, 8), size=(785, 350))

        # campos
        wx.StaticText(painel, -1, "Nome: ", (100, 45), style=wx.ALIGN_CENTRE)
        self.nome = wx.TextCtrl(painel, size=(400, 25), pos=(250, 45))
        self.nome.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))
        wx.StaticText(painel, -1, "Endereco: ", (100, 70), style=wx.ALIGN_CENTRE)
        self.endereco = wx.TextCtrl(painel, size=(400, 25), pos=(250, 70))
        self.endereco.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Telefone: ", (100, 95), style=wx.ALIGN_CENTRE)
        self.telefone = wx.TextCtrl(painel, size=(400, 25), pos=(250, 95))
        self.telefone.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Email: ", (100, 120), style=wx.ALIGN_CENTRE)
        self.email = wx.TextCtrl(painel, size=(400, 25), pos=(250, 120))
        self.email.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Data de nascimento: ", (100, 148), style=wx.adv.DP_SPIN)
        self.data_nasc = wx.adv.DatePickerCtrl(painel, -1, dt=wx.DefaultDateTime, pos=(250, 148)
                                               , validator=wx.DefaultValidator, name='datectrl')
        self.data_nasc.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Região: ", (365, 148), style=wx.ALIGN_CENTRE)
        self.combo_regioes = wx.ComboBox(painel, -1, "", (412, 148), wx.DefaultSize,
                                         ['Norte', 'Nordeste', 'Centro-oeste', 'Sul', 'Sudeste'],
                                         0, wx.DefaultValidator, 'comboestados')
        self.combo_regioes.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        estados = ['ac', 'al', 'ap', 'am', 'ba', 'ce', 'df', 'es', 'go', 'ma', 'mt',
                   'ms', 'mg', 'pa', 'pb', 'pr', 'pe', 'pi', 'rj', 'rn', 'rs', 'ro',
                   'rr', 'sc', 'sp', 'se', 'to']

        wx.StaticText(painel, -1, "Estado: ", (532, 148), style=wx.ALIGN_CENTRE)
        self.combo_estados = wx.ComboBox(painel, -1, "", (584, 148), (70, 33),
                                         estados, 0, wx.DefaultValidator, 'comboestados')
        self.combo_estados.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Sexo: ", (100, 205), style=wx.ALIGN_CENTRE)
        self.sexo = wx.RadioBox(painel, -1, '', (250, 180), choices=['Masculino', 'Feminino'],
                                majorDimension=1, style=wx.RA_SPECIFY_ROWS)
        self.sexo.SetFont(wx.Font(8, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        self.medicamento_controlado = wx.RadioBox(painel, -1, 'Medicamento Controlado ?',
                                                  (490, 185), choices=['Sim', 'Não'],
                                                  majorDimension=1, style=wx.RA_SPECIFY_ROWS)
        self.medicamento_controlado.SetFont(wx.Font(8, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        self.medicamento_controlado.SetSelection(1)  # padrao não

        wx.StaticText(painel, -1, "Nome do Medicamento Controlado : ", (100, 250), style=wx.ALIGN_CENTRE)
        self.nome_medicamento = wx.TextCtrl(painel, size=(310, 25), pos=(350, 250))
        self.nome_medicamento.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        wx.StaticText(painel, -1, "Condição de Saúde Especial ? (cardíaco, hipertenso, diabético) : ",
                      (100, 280), style=wx.ALIGN_CENTRE)
        self.condicao_paciente = wx.TextCtrl(painel, size=(172, 25), pos=(488, 280))
        self.condicao_paciente.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, False))

        # botoes
        self.btn_salvar = wx.Button(painel, -1, 'Salvar', pos=(250, 320))
        self.btn_salvar.SetBitmap(wx.Bitmap(icone_salvar, wx.BITMAP_TYPE_PNG), wx.LEFT)  # seta imagem no botao salvar
        self.btn_atualizar = wx.Button(painel, -1, 'Atualizar', pos=(250, 320))
        self.btn_atualizar.SetBitmap(wx.Bitmap(icone_atualizar, wx.BITMAP_TYPE_PNG), wx.LEFT)  # seta imagem no botao
        self.btn_atualizar.Hide()  # deixa o botao oculto
        self.btn_limpar = wx.Button(painel, -1, 'Limpar', pos=(370, 320))
        self.btn_limpar.SetBitmap(wx.Bitmap(icone_limpar, wx.BITMAP_TYPE_PNG), wx.LEFT)  # seta imagem botao limpar
        self.btn_excluir = wx.Button(painel, -1, 'Excluir', pos=(492,320))
        self.btn_excluir.SetBitmap(wx.Bitmap(icone_deletar,wx.BITMAP_TYPE_PNG), wx.LEFT)
        self.btn_excluir.Hide()

        self.btn_salvar.Bind(wx.EVT_BUTTON, self.salvar)
        self.btn_limpar.Bind(wx.EVT_BUTTON, self.limpar)
        self.btn_atualizar.Bind(wx.EVT_BUTTON, self.atualiza_dados_paciente)
        self.btn_excluir.Bind(wx.EVT_BUTTON, self.excluir_dados_paciente)

        lista_pacientes = wx.StaticBox(painel, -1, 'Listagem', (8, 365), size=(785, 210))
        self.listagem = wx.ListCtrl(painel, -1, style=wx.LC_REPORT, size=(770, 180), pos=(15, 385))

        self.listagem.InsertColumn(0, 'Nome')
        self.listagem.InsertColumn(1, 'Endereço')
        self.listagem.InsertColumn(2, 'Telefone')
        self.listagem.InsertColumn(3, 'Email')
        self.listagem.InsertColumn(4, 'Data Nascimento')
        self.listagem.InsertColumn(5, 'Região')
        self.listagem.InsertColumn(6, 'Estado')
        self.listagem.InsertColumn(7, 'Sexo')
        self.listagem.InsertColumn(8, 'Condição')
        self.listagem.InsertColumn(9, 'Medicamento controlado')
        self.listagem.InsertColumn(10, 'nome medicamento')

        self.listagem.SetColumnWidth(0, 60)
        self.listagem.SetColumnWidth(1, 60)
        self.listagem.SetColumnWidth(2, 60)
        self.listagem.SetColumnWidth(3, 60)
        self.listagem.SetColumnWidth(4, 60)
        self.listagem.SetColumnWidth(6, 60)
        self.listagem.SetColumnWidth(5, 60)
        self.listagem.SetColumnWidth(7, 60)
        self.listagem.SetColumnWidth(8, 60)
        self.listagem.SetColumnWidth(9, 60)
        self.listagem.SetColumnWidth(10, 60)
        self.listagem.Refresh()
        self.listagem.Fit()

        # adiciona evento na listctrl
        self.listagem.Bind(wx.EVT_LIST_ITEM_SELECTED, self.obter_item_tabela)

        barra_estatus = self.CreateStatusBar()
        barra_estatus.SetStatusText("Sistema Único de Saúde")
        # acrescentar metodo para carregar dados do bd aqui
        conexao = obter_conexao()
        if conexao:
            for p in Paciente.objects():
                self.adiciona_item_lista(p)

    # metodo disparado pelo botao
    def salvar(self, event):
        text_nome = self.nome.GetValue()
        text_endereco = self.endereco.GetValue()
        text_telefone = self.telefone.GetValue()
        text_email = self.email.GetValue()
        text_nascimento = self.data_nasc.GetValue()
        text_regiao = self.combo_regioes.GetStringSelection()
        text_estado = self.combo_estados.GetStringSelection()
        text_sexo = self.sexo.GetString(self.sexo.GetSelection())
        text_medicamento_controlado = self.medicamento_controlado \
            .GetString(self.medicamento_controlado.GetSelection())
        text_nome_medicamento = self.nome_medicamento.GetValue()
        text_condicao = self.condicao_paciente.GetValue()
        msg = ''
        paciente = Paciente()  # cria um objeto paciente
        if text_nome == '' or text_nome_medicamento == '' \
                or text_medicamento_controlado == '' or text_condicao == '' \
                or text_endereco == '' or text_telefone == '' \
                or text_email == '' or text_nascimento == '' \
                or text_regiao == '' or text_estado == '' \
                or text_sexo == '':

            msg = 'Favor preencha todos os campos do formulário antes de prosseguir!'
        else:
            # adiciona dados do form ao objeto paciente
            paciente.nome = text_nome
            paciente.endereco = text_endereco
            paciente.estado = text_estado
            paciente.regiao = text_regiao
            paciente.telefone = text_telefone
            paciente.email = text_email
            paciente.sexo = text_sexo
            paciente.nascimento = self._converterDateTimeParaIso(text_nascimento)
            paciente.condicao = text_condicao
            paciente.medicamento_controlado = text_medicamento_controlado
            paciente.nome_medicamento = text_nome_medicamento
            conexao_mongo = obter_conexao()
            esta_conectado = 'Teste'
            if conexao_mongo:
                esta_conectado = True
            else:
                esta_conectado = False
            msg = 'Dados salvos com sucesso!'
            if esta_conectado:
                paciente.save()  # salva no banco
                self.limpar(self)  # apos salvar limpa o formulario
                # aqui vamos criar um metodo para atualiza a tabela
                self.adiciona_item_lista(paciente)
        # mostra mensagem para usuario
        dlg = wx.MessageDialog(None, msg, 'Aviso', wx.OK | wx.ICON_EXCLAMATION)
        dlg.ShowModal()
        disconnect()

    def limpar(self, event):
        self.nome.SetValue('')
        self.endereco.SetValue('')
        self.telefone.SetValue('')
        self.email.SetValue('')
        self.data_nasc.SetValue(wx.DateTime(datetime.datetime.now()))
        self.combo_regioes.SetSelection(0)
        self.combo_estados.SetSelection(0)
        self.sexo.SetSelection(0)
        self.medicamento_controlado.SetSelection(1)
        self.nome_medicamento.SetValue('')
        self.condicao_paciente.SetValue('')
        self.btn_atualizar.Hide()
        self.btn_excluir.Hide()
        self.btn_salvar.Show()

    def _converterDateTimeParaIso(self, date):
        assert isinstance(date, wx.DateTime)
        if date.IsValid():
            ymd = map(int, date.FormatISODate().split('-'))
            return datetime.date(*ymd)
        else:
            return None

    def _converterDataIsoParaDateTime(date):
        assert isinstance(date, (datetime.datetime, datetime.date))
        date_tuple = date.timetuple()
        dmy = (date_tuple[2], date_tuple[1] - 1, date_tuple[0])
        return wx.DateTimeFromDMY(*dmy)

    def adiciona_item_lista(self, paciente):
        contagem_linha = self.listagem.GetItemCount()
        self.listagem.InsertItem(contagem_linha, paciente.nome)
        self.listagem.SetItem(contagem_linha, 1, paciente.endereco)
        self.listagem.SetItem(contagem_linha, 2, paciente.telefone)
        self.listagem.SetItem(contagem_linha, 3, paciente.email)
        self.listagem.SetItem(contagem_linha, 4, str(paciente.nascimento))
        self.listagem.SetItem(contagem_linha, 5, paciente.regiao)
        self.listagem.SetItem(contagem_linha, 6, paciente.estado)
        self.listagem.SetItem(contagem_linha, 7, paciente.sexo)
        self.listagem.SetItem(contagem_linha, 8, paciente.condicao)
        self.listagem.SetItem(contagem_linha, 9, paciente.medicamento_controlado)
        self.listagem.SetItem(contagem_linha, 10, paciente.nome_medicamento)
        self.listagem.Show(False)  # oculta a lista
        self.listagem.Refresh()  # atualiza a lista
        self.listagem.Show(True)  # mostra a linha
        self.listagem.Fit()  # auto resize
        # redefine largura dos items da linha para reexibir scroll
        self.listagem.SetColumnWidth(contagem_linha, 160)

    def obter_item_tabela(self, event):
        linha_tabela = self.listagem.GetFirstSelected()  # obtem o indice do item selecionado
        nome = self.listagem.GetItemText(linha_tabela, 0)  # nome
        endereco = self.listagem.GetItemText(linha_tabela, 1)  # endereco
        telefone = self.listagem.GetItemText(linha_tabela, 2)  # telefone
        email = self.listagem.GetItemText(linha_tabela, 3)  # email
        data = self.listagem.GetItemText(linha_tabela, 4)  # obtem data em formato string
        # data_nascimento formatada como datetime
        nascimento = datetime.datetime.strptime(data, '%Y-%m-%d')
        regiao = self.listagem.GetItemText(linha_tabela, 5)  # regiao
        estado = self.listagem.GetItemText(linha_tabela, 6)  # estado
        sexo = self.listagem.GetItemText(linha_tabela, 7)  # sexo
        condicao = self.listagem.GetItemText(linha_tabela, 8)  # condicao
        medicamento_controlado = self.listagem.GetItemText(linha_tabela, 9)  # medicamento controlado
        medicamento = self.listagem.GetItemText(linha_tabela, 10)  # medicamento
        # oculta o botao salvar e exibe o botao atualizar
        self.btn_atualizar.Hide()
        self.btn_atualizar.Show()
        self.btn_excluir.Show()
        # insere os dados novamente no form
        self.nome.SetValue(nome)
        self.endereco.SetValue(endereco)
        self.telefone.SetValue(telefone)
        self.email.SetValue(email)
        self.data_nasc.SetValue(wx.DateTime(nascimento))
        self.combo_regioes.SetSelection(0)
        self.combo_regioes.SetStringSelection(regiao)
        self.combo_estados.SetStringSelection(estado)


        if str(sexo).strip() == 'Masculino':
            self.sexo.SetSelection(0)
        else:
            self.sexo.SetSelection(1)

        if str(medicamento_controlado).strip() is not None \
                and str(medicamento_controlado).strip().casefold() == 'não'.casefold():
            self.medicamento_controlado.SetSelection(1)
        else:
            self.medicamento_controlado.SetSelection(0)

        self.nome_medicamento.SetValue(medicamento)
        self.condicao_paciente.SetValue(condicao)

    def atualiza_dados_paciente(self, event):
        linha_tabela = self.listagem.GetFirstSelected()  # obtem o indice do item selecionado
        nome_original = self.listagem.GetItemText(linha_tabela, 0)  # nome
        text_nome = self.nome.GetValue()
        text_endereco = self.endereco.GetValue()
        text_telefone = self.telefone.GetValue()
        text_email = self.email.GetValue()
        text_nascimento = str(self.data_nasc.GetValue())
        text_regiao = self.combo_regioes.GetStringSelection()
        text_estado = self.combo_estados.GetStringSelection()
        text_sexo = self.sexo.GetString(self.sexo.GetSelection())
        text_medicamento_controlado = self.medicamento_controlado \
            .GetString(self.medicamento_controlado.GetSelection())
        text_nome_medicamento = self.nome_medicamento.GetValue()
        text_condicao = self.condicao_paciente.GetValue()
        msg = ''
        paciente_atualizacao = Paciente()  # cria um objeto paciente
        # adiciona dados do form ao objeto paciente
        paciente_atualizacao.nome = text_nome
        paciente_atualizacao.endereco = text_endereco
        paciente_atualizacao.estado = text_estado
        paciente_atualizacao.regiao = text_regiao
        paciente_atualizacao.telefone = text_telefone
        paciente_atualizacao.email = text_email
        paciente_atualizacao.sexo = text_sexo
        paciente_atualizacao.nascimento = self._converterDateTimeParaIso(self.data_nasc.GetValue())
        paciente_atualizacao.condicao = text_condicao
        paciente_atualizacao.medicamento_controlado = text_medicamento_controlado
        paciente_atualizacao.nome_medicamento = text_nome_medicamento
        conexao_mongo = obter_conexao()
        esta_conectado = 'Teste'
        if conexao_mongo:
            esta_conectado = True
        else:
            esta_conectado = False

        if esta_conectado:
            # obter registro do bd
            paciente_encontrado = Paciente.objects(nome=nome_original).first()
            try:
                # print(type(paciente_encontrado)) teste de tipo
                paciente_encontrado.update(
                    set__nome=paciente_atualizacao.nome,
                    set__endereco=paciente_atualizacao.endereco,
                    set__estado=paciente_atualizacao.estado,
                    set__regiao=paciente_atualizacao.regiao,
                    set__telefone=paciente_atualizacao.telefone,
                    set__email=paciente_atualizacao.email,
                    set__sexo=paciente_atualizacao.sexo,
                    set__nascimento=paciente_atualizacao.nascimento,
                    set__condicao=paciente_atualizacao.condicao,
                    set__medicamento_controlado=paciente_atualizacao.medicamento_controlado,
                    set__nome_medicamento=paciente_atualizacao.nome_medicamento
                )  # tentativa de atualizacao
                msg = 'Dados Atualizados com sucesso!'
            except Exception as e:
                msg = str(e)

            # apos salvar limpa o formulario
            self.limpar(self)
            # a lista precisa ser reconstruída então deletamos tudo
            self.listagem.DeleteAllItems()
            # relemos o bd e readicionamos os itens a tabela
            for p in Paciente.objects():
                self.adiciona_item_lista(p)
            paciente_atualizacao = None
            paciente_encontrado = None
        else:
            print(esta_conectado)
        # mostra mensagem para usuario
        dlg = wx.MessageDialog(None, msg, 'Aviso', wx.OK | wx.ICON_EXCLAMATION)
        dlg.ShowModal()
        disconnect() # disconecta do banco de dados

    def excluir_dados_paciente(self,event):
        linha_tabela = self.listagem.GetFirstSelected()  # obtem o indice do item selecionado
        nome_original = self.listagem.GetItemText(linha_tabela, 0)  # nome
        text_nome = self.nome.GetValue()
        conexao = obter_conexao()
        paciente_remocao = ''
        if conexao:
            paciente_remocao = Paciente.objects(nome=text_nome).first()
        else:
            wx.MessageDialog(None,'Falha ao Obter conexão','Erro',wx.ICON_ERROR).ShowModal()

        if paciente_remocao != '' and paciente_remocao is not None:
            dlg = wx.MessageDialog(None,'Deseja realmente excluir este Paciente? '\
                                   +paciente_remocao.nome, 'exclusão',
                                   wx.YES_NO|wx.ICON_QUESTION)
            dlg.SetYesNoLabels("&sim","&não")
            resultado = dlg.ShowModal()

            if resultado == wx.ID_YES:
                try:
                    paciente_remocao.delete()
                    self.limpar(self)
                    # a lista precisa ser reconstruída então deletamos tudo
                    self.listagem.DeleteAllItems()
                    # relemos o bd e readicionamos os itens a tabela
                    for p in Paciente.objects():
                        self.adiciona_item_lista(p)
                    disconnect()
                except Exception as e:
                    wx.MessageDialog(None, 'falha ao excluir registro: '\
                                           +str(e), 'Erro', wx.ICON_ERROR).ShowModal()
        else:
            wx.MessageDialog(None, 'Não foi possível encontrar paciente na base de dados',
                             'mensagem',wx.ICON_INFORMATION)

# -------------------------------------------------------------------------------------------------------------------


# classe splash screen

class PostoSplashScreen(SplashScreen):
    """
    cria uma splash screen
    """

    def __init__(self, parent=None):
        # 1 passo obtemos a referencia da imagem da splash
        os.chdir("../src/resources")  # retrocede na hierarquia de diretorios
        imagem = os.getcwd() + "/ps.png"  # diretorio corrente mais arquivo

        # 2 passo usamos a referencia da splash e do icone
        bitmap = wx.Bitmap(name=imagem, type=wx.BITMAP_TYPE_PNG)  # objeto Bitmap

        # 3 passo armz previamente o estilo de pos do splash em var
        estilo = wx.adv.SPLASH_CENTRE_ON_SCREEN | wx.adv.SPLASH_TIMEOUT
        # 4 passo determinamos a duracao da splash em ms
        duracao = 3000

        # 5 passo invocamos o construtor da super classe

        super(PostoSplashScreen, self).__init__(
            bitmap=bitmap,  # imagem
            splashStyle=estilo,  # estilo de posicionamento e tempo
            milliseconds=duracao,  # duracao de tempo em ms
            parent=None,  # possui janela pai ? não
            id=-1,  # identificador numerico qualquer
            pos=wx.DefaultPosition,  # posicao inicial
            size=wx.DefaultSize,  # tamanho
            style=wx.STAY_ON_TOP | wx.BORDER_NONE  # remove bordas e posiciona acima das outras janelas
        )

        # adicionamos evento a janela
        self.Bind(wx.EVT_CLOSE, self.AoSair)

    # metodo a ser executado ao encerrar a splash
    def AoSair(self, event):
        """
         metodo sera executado ao encerramento da splash apos 3 s
        :param self: autoreferencia
        :param event: evento disparado
        :return: nenhum
        """

        # se nao executarmos estas linhas o programa congela
        event.Skip()
        self.Hide()

        # apos executar a splash invocamos a janela principal
        janela = MainWindow()
        janela.CenterOnScreen(wx.BOTH)  # centraliza a janela
        janela.Show()


# -------------------------------------------------------------------------------------------------------------------


class Aplicacao(wx.App):
    """
    classe para iniciar a aplicacao
    """

    def OnInit(self):
        splashScreen = PostoSplashScreen()
        splashScreen.CentreOnScreen(wx.BOTH)  # centraliza na tela
        splashScreen.Show(True)
        return True


if __name__ == "__main__":
    """
    metodo principal do programa
    """
    aplicacao = Aplicacao()
    aplicacao.MainLoop()
